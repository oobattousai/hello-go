FROM golang:1.20.3-buster as builder
WORKDIR /app

COPY src/ .
RUN ls -l

RUN go build -o hello && chmod +x hello

FROM scratch as production

COPY --from=builder /app/hello /

CMD [ "/hello" ]